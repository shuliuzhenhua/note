配置基础

ThinkPHP遵循惯例重于配置的原则，系统会按照下面的顺序来加载配置文件（配置的优先顺序从右到左）。

惯例配置->应用配置->模块配置->动态配置\

惯例配置：核心框架内置的配置文件，无需更改。
应用配置：每个应用的全局配置文件（框架安装后会生成初始的应用配置文件），有部分配置参数仅能在应用配置文件中设置。
模块配置：每个模块的配置文件（相同的配置参数会覆盖应用配置），有部分配置参数模块配置是无效的，因为已经使用过。
动态配置：主要是指在控制器或者行为中进行（动态）更改配置，该配置方式只在当次请求有效，因为不会保存到配置文件中

惯例配置：C:\wamp64\www\tp5\thinkphp\convention.php
应用配置：C:\wamp64\www\tp5\config\
模块配置：C:\wamp64\www\tp5\application\index\config\
动态配置：

如果你需要统一管理所有的配置文件，那么可以把模块目录下面的config目录移动到应用配置目录下面改为模块子目录的方式，调整后的配置目录的结构如下：
├─application（应用目录）
├─config（配置目录）
│  ├─module （模块配置目录）
│  │  ├─database.php    数据库配置
│  │  ├─cache           缓存配置
│  │  └─ ...            
│  │
│  ├─app.php            应用配置
│  ├─cache.php          缓存配置
│  ├─cookie.php         Cookie配置
│  ├─database.php       数据库配置
│  ├─log.php            日志配置
│  ├─session.php        Session配置
│  ├─template.php       模板引擎配置
│  └─trace.php          Trace配置
│  
├─route（路由配置目录）
│  ├─route.php          路由定义文件
│  └─ ...               更多路由定义文件

二级配置

return [
    'user'  =>  [
        'type'  =>  1,
        'name'  =>  'thinkphp',
    ],
    'db'    =>  [
        'type'      =>  'mysql',
        'user'      =>  'root',
        'password'  =>  '',
    ],
];

环境变量定义
可以在应用的根目录下定义一个特殊的.env环境变量文件

APP_DEBUG =  true
APP_TRACE =  true

DATABASE_USERNAME =  root
DATABASE_PASSWORD =  123456

或者

[DATABASE]
USERNAME =  root
PASSWORD =  123456

配置获取

在需要使用的文件
use Config;

然后 echo Config::get('配置参数1'); 只能查询app配置下面

读取某个一级配置下所有配置
Config::pull('app');

读取所有配置 

dump(Config::get());

判断是否存在某个设置参数：

Config::has('配置参数2');

助手函数

config

echo config('配置参数1');

5.1的配置参数全部采用二级配置的方式（默认一级配置为app），所以当你使用config('name')的时候其实相当于使用
config('app.name')

支持获取多级配置参数值，直接使用（必须从一级开始写）

config('app.name1.name2')

获取某个一级配置的所有参数可以使用

config('app.');

或者你需要判断是否存在某个设置参数：

config('?配置参数2');

动态设置

config('app.name','twodogegg');

config([
    'app_trace'=>true,
    'show_error_msg'=>true
],'app');

系统配置文件


配置文件名	描述
app.php	应用配置
cache.php	缓存配置
cookie.php	Cookie配置
database.php	数据库配置
log.php	日志配置
session.php	Session配置
template.php	模板引擎配置
trace.php	页面Trace配置
paginate.php	分页配置


路由定义

绑定域名定义：

Route::domain('data', function () {
    // 动态注册域名的路由规则
    Route::rule('goods', 'admin/index/goods');
    Route::rule(':user', 'index/user/info');
});